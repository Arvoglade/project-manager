package ru.gashev.test.advenjineering.projectmanager.entity;

public enum TaskType {

    MANAGER,
    TECHNICAL_SPECIALIST
}
