package ru.gashev.test.advenjineering.projectmanager.entity;


public enum TaskStatus {

    NEW,
    PROGRESS,
    DONE

}
