package ru.gashev.test.advenjineering.projectmanager.entity;

public enum AuthorityType {
    USER,
    ADMIN
}
